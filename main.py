from gg_search import GoogleSearch
ggsearch = GoogleSearch()
from relevance_ranking import rel_ranking

def main():
    question = 'ai là người giàu nhất Việt Nam'

    # Using google to find relevant documents
    links, documents = ggsearch.search(question)

    # Find relevant passages from documents
    passages = rel_ranking(question, documents)

    # Select top 40 paragraphs
    passages = passages[:40]

if __name__ == "__main__":
    main()